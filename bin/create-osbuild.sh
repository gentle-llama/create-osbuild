#!/bin/bash

set -euxo pipefail

source lib/create-osbuild.sh

set_vars

workaound_for_gpg

install_dependencies

set_osbuild_options

cd "${MANIFEST_DIR}"

build_image

echo "[+] Moving the generated image"
mkdir -p "$(dirname "$IMAGE_FILE")"
mv "$DISK_IMAGE" "$IMAGE_FILE"

build_metadata

cleanup

echo "The final image is here: ${IMAGE_FILE}"
echo "Information about image is in ${IMAGE_FILE%.*}.json"
