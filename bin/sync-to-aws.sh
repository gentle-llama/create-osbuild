#!/bin/bash
set -xeo pipefail

echo "[+] Install dependencies"
dnf install -y jq python3-pip
pip3 install awscli

echo "[+] Configure AWS settings"
$AWS_CLI configure set default.region "$AWS_REGION"
$AWS_CLI configure set default.output json

# .raw file is needed for import as AMI in S3
if [[ "${BUILD_FORMAT}" == "img" ]]; then
  BUILD_FORMAT="raw"
fi

IMAGE_FILE="${IMAGE_KEY}.${BUILD_FORMAT}"
if [ ! -r "${DOWNLOAD_DIRECTORY}/${IMAGE_FILE}" ]; then
  echo "Error: the file ${IMAGE_FILE} doesn't exist"
  exit 1
fi
if [[ "${BUILD_FORMAT}" == "qcow2" ]] || [[ "${BUILD_TARGET}" == "rpi4"  ]]; then
  xz "${DOWNLOAD_DIRECTORY}/${IMAGE_FILE}"
  IMAGE_FILE="${IMAGE_FILE}.xz"           # add ".xz" to file extension for simplicity
fi

if [[ "${SAMPLE_IMAGE}" == "True" ]]; then
  S3_UPLOAD_PREFIX="${UUID}/sample-images"
else
  S3_UPLOAD_PREFIX="${UUID}/non-sample-images"
fi
echo "[+] Uploading raw image to 's3://${S3_BUCKET_NAME}/${S3_KEY}'"
$AWS_CLI s3 cp "${DOWNLOAD_DIRECTORY}/${IMAGE_FILE}" \
	       "s3://${S3_BUCKET_NAME}/${S3_UPLOAD_PREFIX}/" \
	       --only-show-errors

$AWS_CLI s3 cp "${DOWNLOAD_DIRECTORY}/${IMAGE_KEY}.json" \
	       "s3://${S3_BUCKET_NAME}/${S3_UPLOAD_PREFIX}/" \
	       --only-show-errors

# Only import images used for testing, basically the non-compressed images
if [[ "${IMAGE_FILE}" == *".xz" ]]; then
  echo "[+] The image is not for testing, it won't be imported as AMI"
  exit 0
fi

if [[ "${STREAM}" == "downstream" ]]; then
	echo "[+] Disable (for now) te AMI importation for Downstream"
  exit 0
fi

echo "[+] Import snapshot to EC2"

S3_KEY="${S3_UPLOAD_PREFIX}/${IMAGE_FILE}"
IMPORT_SNAPSHOT_ID=$($AWS_CLI ec2 import-snapshot --disk-container Format=raw,UserBucket="{S3Bucket=${S3_BUCKET_NAME},S3Key=${S3_KEY}}" | jq -r .ImportTaskId)
set +x # Disable the debug during the loop

echo "[+] Waiting for snapshot $IMPORT_SNAPSHOT_ID import"

until [[ "$status" == "completed" ]]; do
  status=$($AWS_CLI ec2 describe-import-snapshot-tasks --import-task-ids "$IMPORT_SNAPSHOT_ID" | jq -r .ImportSnapshotTasks[0].SnapshotTaskDetail.Status)
  echo -n "."
done

set -x # Enable the debug again

echo "snapshot status: $status"

$AWS_CLI ec2 describe-import-snapshot-tasks --import-task-ids "$IMPORT_SNAPSHOT_ID"
SNAPSHOT_ID=$($AWS_CLI ec2 describe-import-snapshot-tasks --import-task-ids "$IMPORT_SNAPSHOT_ID" | jq -r .ImportSnapshotTasks[0].SnapshotTaskDetail.SnapshotId)

$AWS_CLI ec2 create-tags --resources "$SNAPSHOT_ID" --tags Key=ServiceComponent,Value=Automotive Key=ServiceOwner,Value=A-TEAM Key=ServicePhase,Value=Prod Key=FedoraGroup,Value=ci

echo "[+] Copy snapshot from ${AWS_REGION} to ${AWS_TF_REGION}"
SNAPSHOT_ID=$($AWS_CLI ec2 copy-snapshot --source-region "${AWS_REGION}" --source-snapshot-id "${SNAPSHOT_ID}" --region="${AWS_TF_REGION}" | jq -r .SnapshotId)

echo "[+] Waiting for copy to complete"

set +x # Disable the debug during the loop
status=""
until [[ "$status" == "completed" ]]; do
	status=$($AWS_CLI ec2 describe-snapshots --region "${AWS_TF_REGION}" --snapshot-ids "${SNAPSHOT_ID}" | jq -r .Snapshots[0].State)
	echo -n "."
done
set -x # Enable the debug again
echo "snapshot status: $status"

echo "[+] Register AMI from snapshot"
if [[ "${ARCH}" == "aarch64" ]]; then
	ARCH="arm64"
fi
IMAGE_ID=$(
	$AWS_CLI ec2 register-image --name "${IMAGE_KEY}" \
		--region "${AWS_TF_REGION}" \
		--architecture "${ARCH}" \
		--virtualization-type hvm \
		--root-device-name "/dev/sda1" \
		--ena-support \
		--boot-mode uefi \
		--block-device-mappings "[
   {
       \"DeviceName\": \"/dev/sda1\",
       \"Ebs\": {
           \"SnapshotId\": \"$SNAPSHOT_ID\"
       }
   }]" | jq -r .ImageId
)

$AWS_CLI ec2 create-tags --resources "$IMAGE_ID" --region="${AWS_TF_REGION}" --tags Key=ServiceComponent,Value=Artemis Key=ServiceName,Value=Artemis Key=AppCode,Value=ARR-001 Key=ServiceOwner,Value=TFT Key=ServicePhase,Value=Prod Key=PIPELINE_RUN_ID,Value="${UUID}"

# Wait for image registration
echo "[+] Waiting for image $IMAGE_ID registration"
set +x # Disable the debug during the loop
until [[ "$state" == "available" ]]; do
	state=$($AWS_CLI ec2 describe-images --image-ids "${IMAGE_ID}" --region "${AWS_TF_REGION}" | jq -r .Images[0].State)
	echo -n "."
done
set -x # Enable the debug again

# Give permissions to the Testing Farm provisioner account
echo "[+] Granting permissions for $IMAGE_ID to the Testing Farm provisioner"
aws ec2 modify-image-attribute \
	--image-id "${IMAGE_ID}" \
	--region "${AWS_TF_REGION}" \
	--launch-permission "Add=[{UserId=${AWS_TF_ACCOUNT_ID}}]"

echo "image state: $state"
$AWS_CLI ec2 describe-images --image-ids "$IMAGE_ID" --region "${AWS_TF_REGION}"
