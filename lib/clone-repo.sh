#!/bin/bash

GIT="git"

install_git() {
  dnf install -y git
}

check_ssl() {
  if [[ "${SSL_VERIFY}" == "false" ]]; then
    GIT="GIT_SSL_NO_VERIFY=true git"
  fi
}

clone_sample_images_repo() {
  check_ssl
  $GIT clone "${REPO_URL}" "${OSBUILD_DIR}"
  cd "${OSBUILD_DIR}"
  $GIT checkout -b upstream "origin/${REVISION}"
}
