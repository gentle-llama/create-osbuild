#!/bin/bash

# Get OS data.
source /etc/os-release

set_vars() {
  DISTRO="${OS_PREFIX}${OS_VERSION}"
  # DISK_IMAGE is the target for the Makefile and the name of the resulted file.
  # Example: cs9-qemu-minimal-ostree.aarch64.img
  DISK_IMAGE="${DISTRO}-${BUILD_TARGET}-${IMAGE_NAME}-${IMAGE_TYPE}.${ARCH}.${BUILD_FORMAT}"
  # .raw file is needed for import as AMI in S3
  if [[ "${BUILD_FORMAT}" == "img" ]]; then
    BUILD_FORMAT="raw"
  fi
  IMAGE_FILE=${IMAGE_FILE:-"${DOWNLOAD_DIRECTORY}/${IMAGE_KEY}.${BUILD_FORMAT}"}
  STREAM=${STREAM:-"upstream"}
  MPP_ARGS=""
  MANIFEST_DIR="osbuild-manifests/osbuild-manifests"
  DOWNSTREAM_DISTRO_TEMPLATE="files/downstream.yml"
  DOWNSTREAM_DISTRO_FILE="${MANIFEST_DIR}/distro/${DISTRO}.ipp.yml"
}

workaound_for_gpg() {
  #Workaround to solve GPG check failed
  update-crypto-policies --set LEGACY
}

install_dependencies() {
  # Add Osbuild upstream yum repo to install the latest version
  curl --output /etc/yum.repos.d/osbuild-centos-stream-9.repo \
    https://copr.fedorainfracloud.org/coprs/g/osbuild/osbuild/repo/centos-stream-9/group_osbuild-osbuild-centos-stream-9.repo

  dnf -y install osbuild osbuild-tools osbuild-ostree make skopeo
}

# Create the distro file for the default mpp values for downstream
create_downstream_distro_file() {
  export DISTRO DOWNSTREAM_COMPOSE_URL
  envsubst '${DISTRO},${DOWNSTREAM_COMPOSE_URL}' \
	  < "$DOWNSTREAM_DISTRO_TEMPLATE" \
	  > "$DOWNSTREAM_DISTRO_FILE"
}

# Add options to the osbuild to enable the SSH access to the image
enable_ssh() {
  OS_OPTIONS+=("ssh_permit_root_login=true")
  OS_OPTIONS+=("extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]")
}

add_ssh_key() {
  local ssh_public_key="${1}"
  MPP_ARGS="-D 'root_ssh_key=\"${ssh_public_key}\"'"
}

# Use the yum repo from the pipeline
override_yum_repo_with_product_build () {
  YUM_REPO_URL="${CI_REPOS_ENDPOINT}/${UUID}/repos/${DISTRO}"
  OS_OPTIONS+=("distro_baseurl=\"${YUM_REPO_URL}\"")
  OS_OPTIONS+=("distro_automotive_repos=[]")
  OS_OPTIONS+=("distro_crb_repos=[]")
  OS_OPTIONS+=("distro_debug_repos=[]")
  OS_OPTIONS+=("distro_repos=[{\"id\":\"auto\",\"baseurl\":\"${YUM_REPO_URL}/${ARCH}/os/\"}]")
}

set_osbuild_options() {
  if [[ "${PACKAGE_SET}" == "${PRODUCT_BUILD_PREFIX}" ]]; then
    override_yum_repo_with_product_build
  fi
  
  # Use 'True' instead of 'yes' due a bug in tmt or Testing Farm
  if [[ "${TEST_IMAGE}" == "True" ]]; then
    enable_ssh
    add_ssh_key "${SSH_KEY}"
  fi
  
  if [[ "${STREAM}" == "downstream" ]]; then
    create_downstream_distro_file
  fi
}

build_image() {
  make "${DISK_IMAGE}" \
       DEFINES="${OS_OPTIONS[*]}" \
       MPP_ARGS="${MPP_ARGS}"
}

build_metadata() {
  # record some details of the input manifest, and disk image in json
  cat <<EOF | sudo tee -a "${IMAGE_FILE%.*}".json
{
  "image_file": "${IMAGE_FILE}",
  "arch": "${ARCH}",
  "os_version": "${OS_PREFIX}${OS_VERSION}",
  "build_type": "${BUILD_TYPE}",
  "image_name": "${IMAGE_NAME}",
  "image_type": "${IMAGE_TYPE}",
  "variables": [
    {
      "UUID": "${UUID}",
      "REPO_URL": "${REPO_URL}",
      "REVISION": "${REVISION}",
      "ID": "${ID}"
    }
  ]
}
EOF
}

# Clean up
cleanup() {
  echo "[+] Cleaning up"
  make clean
}
