Describe 'clone-repo'
  Include ./lib/clone-repo.sh

  Describe 'install dependencies'
    Mock dnf
      echo "dnf $*"
    End

    It 'installs git'
      When call install_git
        The output should include "git"
    End
  End

  Describe 'check_ssl'

    It 'disable ssl to git if SSL_VERIFY is false'
      SSL_VERIFY="false"
      When call check_ssl
        The value "${GIT}" should start with "GIT_SSL_NO_VERIFY=true"
	The value "${GIT}" should include "git"
    End

    It 'do not disable ssl to git if SSL_VERIFY is true'
      SSL_VERIFY="true"
      When call check_ssl
        The value "${GIT}" should not start with "GIT_SSL_NO_VERIFY=true"
	The value "${GIT}" should include "git"
    End
  End

  Describe 'clone_sample_images_repo'

    Mock git
      echo "git $*"
    End
    Mock "GIT_SSL_NO_VERIFY=true"
      echo "GIT_SSL_NO_VERIFY=true $*"
    End

    It 'disable ssl to git if SSL_VERIFY is false'
      SSL_VERIFY="false"
      When call clone_sample_images_repo
        The variable GIT should be defined
	The output should start with "GIT_SSL_NO_VERIFY=true"
    End

    It 'do not disable ssl to git if SSL_VERIFY is true'
      SSL_VERIFY="true"
      When call clone_sample_images_repo
        The variable GIT should be defined
        The output should not start with "GIT_SSL_NO_VERIFY=true"
    End
  End
End
