Describe 'create-osbuild'
  Include ./lib/create-osbuild.sh
  SSH_KEY="ssh-rsa my-public-key"
  CI_REPOS_ENDPOINT="http://my.fake.repo"
  UUID="XXX"
  ARCH="ppc64"
  DISTRO="cs9"

  Describe 'set variables'
    OS_PREFIX=cs
    OS_VERSION=9
    BUILD_TARGET=qemu
    IMAGE_NAME=minimal
    IMAGE_TYPE=ostree
    ARCH=aarch64
    BUILD_FORMAT=img

    It 'sets the proper DISTRO variable'
      When call set_vars
      The value "${DISTRO}" should eq "cs9"
      The value "${DISTRO}" should not eq "cs8"
      The value "${DISTRO}" should not eq "rhel9"
    End

    It 'sets the proper DISK_IMAGE'
      When call set_vars
      The value "${DISK_IMAGE}" should eq "cs9-qemu-minimal-ostree.aarch64.img"
    End

    It 'sets the proper format for raw images'
      When call set_vars
      The value "${DISK_IMAGE}" should end with ".img"
      The value "${IMAGE_FILE}" should end with ".raw"
      The value "${IMAGE_FILE}" should not end with ".qcow2"
    End

    It 'sets the proper format for qcow2 images'
      BUILD_FORMAT=qcow2
      When call set_vars
      The value "${DISK_IMAGE}" should end with ".qcow2"
      The value "${IMAGE_FILE}" should end with ".qcow2"
      The value "${IMAGE_FILE}" should not end with ".raw"
    End

    It 'sets a template for the dirtro file that exist'
      When call set_vars
      The path "${DOWNSTREAM_DISTRO_TEMPLATE}" should be file
    End
  End

  Describe 'install dependencies'
    Mock dnf
      echo "dnf $*"
    End
    Mock curl
      :
    End

    It 'installs osbuild dependencies'
      When call install_dependencies
        The output should include "osbuild"
        The output should include "osbuild-tools"
        The output should include "osbuild-ostree"
    End

    It 'installs make'
      When call install_dependencies
        The output should include "make"
    End

    It 'installs skopeo for the container image'
      When call install_dependencies
        The output should include "skopeo"
    End
  End

  Describe 'enable SSH'
    It 'adds ssh packages and enable ssh in the image'
      When call enable_ssh
      The value "${OS_OPTIONS[*]}" should include "ssh_permit_root_login=true"
      The value "${OS_OPTIONS[*]}" should include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
    End

    It 'adds Testing Farm public ssh key'
      When call add_ssh_key "$SSH_KEY"
      The value "$MPP_ARGS" should equal "-D 'root_ssh_key=\"ssh-rsa my-public-key\"'"
    End
  End

  Describe 'use product build at yum repo'
    It 'sets product build as distro_baseurl'
      When call override_yum_repo_with_product_build
      The value "${OS_OPTIONS[*]}" should include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
    End

    It 'clears all the predefined repos (automotive, crb and debug)'
      When call override_yum_repo_with_product_build
      The value "${OS_OPTIONS[*]}" should include "distro_automotive_repos=[]"
      The value "${OS_OPTIONS[*]}" should include "distro_crb_repos=[]"
      The value "${OS_OPTIONS[*]}" should include "distro_debug_repos=[]"
    End

    It 'sets new repo "auto" that point to the product build plus the current arch'
      When call override_yum_repo_with_product_build
      The value "${OS_OPTIONS[*]}" should include "distro_repos=[{\"id\":\"auto\",\"baseurl\":\"http://my.fake.repo/XXX/repos/cs9/ppc64/os/\"}]"
    End

    It 'should not set the CS9 repo as distro_baseurl'
      When call override_yum_repo_with_product_build
      The value "${OS_OPTIONS[*]}" should not include "distro_baseurl=\"http://mirror.stream.centos.org/9-stream\""
    End
  End

  Describe 'set_osbuild_options'
    Describe 'set options for a build normal sample image (not for testing)'
      SAMPLE_IMAGE=True
      TEST_IMAGE=False
  
      It 'should no have the OS_OPTIONS for testing'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should not include "ssh_permit_root_login=true"
        The value "${OS_OPTIONS[*]}" should not include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
      End
  
      It 'should not include any ssh key'
        When call set_osbuild_options
        The value "$MPP_ARGS" should be blank
      End
  
      It 'uses the product build as distro_baseurl'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End
    End
  
    Describe 'set options for a build sample image (for testing)'
      SAMPLE_IMAGE=True
      TEST_IMAGE=True
    
      It 'calls make with the OS_OPTIONS for testing'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should include "ssh_permit_root_login=true"
        The value "${OS_OPTIONS[*]}" should include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
        The value "$MPP_ARGS" should include "-D 'root_ssh_key=\"ssh-rsa my-public-key\"'"
      End
    
      It 'should not use the wrong ssh key'
        When call set_osbuild_options
        The value "$MPP_ARGS" should not include "-D 'root_ssh_key=\"ssh-rsa my-wrong-key\"'"
      End
    
      It 'uses the product build as distro_baseurl'
        When call set_osbuild_options
        The value "${OS_OPTIONS[*]}" should include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End
    End
  End
  
  Describe 'build_image'
    Mock make
      echo "make $*"
    End

    Describe 'build sample image (not for testing or for rpi4)'
      SAMPLE_IMAGE=True
      TEST_IMAGE=False
      PACKAGE_SET=autosd9
      PRODUCT_BUILD_PREFIX=autosd9
      set_osbuild_options
  
      It 'uses the product build as distro_baseurl'
        When call build_image
        The output should include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End
  
      It 'has no the ssh enabled'
        When call build_image
        The output should not include "ssh_permit_root_login=true"
        The output should not include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
      End
  
      It 'has not the Testing Farm public ssh key'
        When call build_image
        The output should not include "-D 'root_ssh_key=\"ssh-rsa my-public-key\"'"
      End
    End

    Describe 'build sample image (for rpi4)'
      SAMPLE_IMAGE=True
      TEST_IMAGE=False
      PACKAGE_SET=autosd9
      PRODUCT_BUILD_PREFIX=autosd9
      set_osbuild_options

      It 'uses the product build as distro_baseurl'
        When call build_image
        The output should include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End

      It 'has no the ssh enabled'
        When call build_image
        The output should not include "ssh_permit_root_login=true"
        The output should not include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
      End

      It 'has not the Testing Farm public ssh key'
        When call build_image
        The output should not include "-D 'root_ssh_key=\"ssh-rsa my-public-key\"'"
      End
    End

    Describe 'build sample image (for testing)'
      SAMPLE_IMAGE=True
      TEST_IMAGE=True
      PACKAGE_SET=autosd9
      PRODUCT_BUILD_PREFIX=autosd9
      set_osbuild_options

      It 'uses the product build as distro_baseurl'
        When call build_image
        The output should include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End

      It 'has the ssh enabled'
        When call build_image
        The output should include "ssh_permit_root_login=true"
        The output should include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
      End

      It 'has the Testing Farm public ssh key'
        When call build_image
        The output should include "-D 'root_ssh_key=\"ssh-rsa my-public-key\"'"
      End

      It 'should not use the wrong ssh key'
        When call build_image
        The output should not include "-D 'root_ssh_key=\"ssh-rsa my-wrong-key\"'"
      End
    End

    Describe 'build non sample image (not for testing)'
      SAMPLE_IMAGE=False
      TEST_IMAGE=False
      PACKAGE_SET=cs9
      PRODUCT_BUILD_PREFIX=autosd9
      set_osbuild_options
  
      It 'does not use the product build as distro_baseurl'
        When call build_image
        The output should not include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End
  
      It 'has no the ssh enabled'
        When call build_image
        The output should not include "ssh_permit_root_login=true"
        The output should not include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
      End
  
      It 'has not the Testing Farm public ssh key'
        When call build_image
        The output should not include "-D 'root_ssh_key=\"ssh-rsa my-public-key\"'"
      End
    End

    Describe 'build non sample image (for testing)'
      SAMPLE_IMAGE=False
      TEST_IMAGE=True
      PACKAGE_SET=cs9
      PRODUCT_BUILD_PREFIX=autosd9
      set_osbuild_options
  
      It 'does not use the product build as distro_baseurl'
        When call build_image
        The output should not include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End
  
      It 'has the ssh enabled'
        When call build_image
        The output should include "ssh_permit_root_login=true"
        The output should include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
      End
  
      It 'adds the Testing Farm public ssh key'
        When call build_image
        The output should include "-D 'root_ssh_key=\"ssh-rsa my-public-key\"'"
      End
  
      It 'should not use the wrong ssh key'
        When call build_image
        The output should not include "-D 'root_ssh_key=\"ssh-rsa my-wrong-key\"'"
      End
    End

    Describe 'build the QA image is a sample image for testing, but uses CS9'
      SAMPLE_IMAGE=True
      TEST_IMAGE=True
      PACKAGE_SET=cs9
      PRODUCT_BUILD_PREFIX=autosd9
      set_osbuild_options

      It 'does not use the product build as distro_baseurl'
        When call build_image
        The output should not include "distro_baseurl=\"http://my.fake.repo/XXX/repos/cs9\""
      End

      It 'has the ssh enabled'
        When call build_image
        The output should include "ssh_permit_root_login=true"
        The output should include "extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]"
      End

      It 'adds the Testing Farm public ssh key'
        When call build_image
        The output should include "-D 'root_ssh_key=\"ssh-rsa my-public-key\"'"
      End

      It 'should not use the wrong ssh key'
        When call build_image
        The output should not include "-D 'root_ssh_key=\"ssh-rsa my-wrong-key\"'"
      End
    End
  End

  Describe 'create downstream distro file'
    DISTRO="rhel9"
    DOWNSTREAM_COMPOSE_URL="https://my-fake.repo/rhel9"
    DOWNSTREAM_DISTRO_TEMPLATE="files/downstream.yml"
    DOWNSTREAM_DISTRO_FILE="rhel9.yml"
    cleanup() { rm -f "$DOWNSTREAM_DISTRO_FILE"; }
    BeforeEach 'cleanup'
    AfterEach 'cleanup'

    It 'downstream distro file has the right prefix'
      When call create_downstream_distro_file
      The contents of file "${DOWNSTREAM_DISTRO_FILE}" should include "distro_name: rhel9"
    End

    It 'downstream distro file has the right baseurl'
      When call create_downstream_distro_file
      The contents of file "${DOWNSTREAM_DISTRO_FILE}" should include "distro_baseurl: https://my-fake.repo/rhel9"
    End
  End

End
