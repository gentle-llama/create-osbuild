[![pipeline status](https://gitlab.com/gentle-llama/create-osbuild/badges/main/pipeline.svg)](https://gitlab.com/gentle-llama/create-osbuild/-/commits/main)
[![coverage report](https://gitlab.com/gentle-llama/create-osbuild/badges/main/coverage.svg)](https://gitlab.com/gentle-llama/create-osbuild/-/commits/main)

# create-osbuild

**create-osbuild** is a shellscript wrapper for [osbuild] and the template
system created for the [Automotive sample images]. It's packaged in a [tmt]
plan, so it can run in [Testing Farm].

This is part of the [Automotive toolchain pipeline] and is used for building
**Automotive** images.

## Local testing and debugging

**create-osbuild** is wrapped with [tmt] to be able to run the job in
[Testing Farm], but `tmt` allows you to run it locally as well.


### Run tmt locally

With `tmt` you can run **create-osbuild** in a VM (virtual machine) and test
the building of images in your laptop.

First, you'll need to install `tmt` and the virtual plugin:

```bash
dnf install tmt-all
```

To run the code in the VM use the following command:

```bash
tmt run -a -vvv provision -h virtual -i centos-stream-9 plans
```

If you need (or want) to test different parameters for the build, you can do it
at this file, which is the definition for the `tmt` local plan:

[plans/local.fmf](plans/local.fmf)

### Debug locally using tmt

In case of an error, you can run `tmt` with these extra options to login
into the system after the error, so you can debug the error:

```bash
tmt run -a -vvv provision -h virtual -i centos-stream-9 plans -n local login --step execute --when error
```

If the problem is a `fail` at some of the _tests_, you can use `fail` instead
of `error` at the `when` statment:

```bash
tmt run -a -vvv provision -h virtual -i centos-stream-9 plans -n local login --step execute --when fail
```

## Tests

For testing, **create-osbuild** uses [shellspec] and for code coverage [Kcov].

Please, make sure that all the tests run before add any new code or change. To
run the tests use the following command:

```bash
shellspec
```

And to get also the code coverage:

```bash
shellspec --kcov
```

### Run using a container

Probably the easiest way to run the tests is using the container provided by
this repository. First you'll need to build the container:

```bash
podman build -t create-osbuild .
```

Then you can using by running the folowing command:

```bash
podman run -it --rm -v .:/code -w /code create-osbuild shellspec
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to
discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[Apache 2](LICENSE)

[osbuild]: https://github.com/osbuild/osbuild
[tmt]: https://tmt.readthedocs.io/
[Automotive sample images]: https://gitlab.com/CentOS/automotive/sample-images
[Testing Farm]: https://docs.testing-farm.io
[Automotive toolchain pipeline]: https://gitlab.com/gentle-llama/pipelines-as-code
[shellspec]: https://shellspec.info/
[Kcov]: https://github.com/SimonKagstrom/kcov
